# qc

qc is a minimalist desktop calculator app written
in blazingly fast™ Rust.

![sqrt(2^64) = 4294967296](qc.png)

## Syntax

All of your favorite mathematical operators are supported:

 - `+`: add
 - `-`: subtract
 - `*`: multiply
 - `/`: divide
 - `//`: computes integer division between two numbers. If the arguments
   are not both integers, equivalent to `trunc(x/y)`
 - `%`: modulo
 - `^`: exponentiation

Adjacent terms are multiplied:
 - `2pi`
 - `(4 + 5)(6 + 7)`

### Functions

There are also a number of numerical functions:
 - `sqrt`
 - `log` (or `ln`, `log2` and `log10` also available)
 - `sin`
 - `cos`
 - `tan`
 - `asin` (or `arcsin`)
 - `acos` (or `acos`)
 - `atan` (or `arctan`)
 - `floor`
 - `ceil` (or `ceiling`)
 - `round`
 - `trunc` (or `truncate`)
 - `abs`

Parentheses are optional when calling a function:

 - `cos pi` (equivalent to `cos(pi)`)
 - `cos 2pi` (equivalent to `(cos(2))*pi`)
